# pip install selenium
# Install the newest chrome version, should be 122
# Install chromedriver version 122
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager

class AcceptTestsVH(unittest.TestCase):

	options = webdriver.ChromeOptions()
	service = Service(ChromeDriverManager().install())
	options.add_argument("--no-sandbox")
	options.add_argument("--headless")
	options.add_argument("--disable-gpu")
	options.add_argument("--disable-dev-shm-usage")
	driver = webdriver.Chrome(service=service, options=options)
	driver.get("https://www.veteranhaven.me/")

	def setUp(self):
		options = webdriver.ChromeOptions()
		options.add_argument("--no-sandbox")
		options.add_argument("--headless")
		options.add_argument("--disable-gpu")
		options.add_argument("--disable-dev-shm-usage")
		self.driver = webdriver.Chrome(options=options)

	# Tests the home button on splash
	def testSplash(self):
		self.driver.get("https://www.veteranhaven.me/")
		self.driver.implicitly_wait(10)
		self.driver.find_element(By.XPATH, '//a[contains(@href, "/")]').click()
		try:
			self.assertEqual("https://www.veteranhaven.me/", self.driver.current_url)
			print("Passed Splash Test \u2705")
		except AssertionError:
			print("Failed Splash Test")

	# Tests the about button on splash
	def testAbout(self):
		self.driver.get("https://www.veteranhaven.me/")
		self.driver.implicitly_wait(10)
		self.driver.find_element(By.XPATH, '//a[contains(@href, "/about")]').click()
		try:
			self.assertEqual("https://www.veteranhaven.me/about", self.driver.current_url)
			print("Passed About Test \u2705")
		except AssertionError:
			print("Failed About Test")
	
	# Tests the housing button on splash
	def testHousing(self):
		self.driver.get("https://www.veteranhaven.me/")
		self.driver.implicitly_wait(10)
		self.driver.find_element(By.XPATH, '//a[contains(@href, "/housing")]').click()
		try:
			self.assertEqual("https://www.veteranhaven.me/housing", self.driver.current_url)
			print("Passed Housing Test \u2705")
		except AssertionError:
			print("Failed Housing Test \U0001F645")

	# Tests the education button on splash
	def testEducation(self):
		self.driver.get("https://www.veteranhaven.me/")
		self.driver.implicitly_wait(10)
		self.driver.find_element(By.XPATH, '//a[contains(@href, "/education")]').click()
		try:
			self.assertEqual("https://www.veteranhaven.me/education", self.driver.current_url)
			print("Passed Education Test \u2705")
		except AssertionError:
			print("Failed Education Test \U0001F645")

	# Tests the healthcare button on splash
	def testHealthcare(self):
		self.driver.get("https://www.veteranhaven.me/")
		self.driver.implicitly_wait(10)
		self.driver.find_element(By.XPATH, '//a[contains(@href, "/healthcare")]').click()
		try:
			self.assertEqual("https://www.veteranhaven.me/healthcare", self.driver.current_url)
			print("Passed Healthcare Test \u2705")
		except AssertionError:
			print("Failed Healthcare Test \U0001F645")

	# Tests the learn more button on instance card
	def testHousingInstanceCard(self):
		self.driver.get("https://www.veteranhaven.me/housing")
		self.driver.implicitly_wait(10)
		# self.driver.find_element(By.XPATH, '//*[@id="root"]/div/div[2]/div[3]/div[3]/div/div/div[3]/a[1]').click()
		self.driver.find_element(By.CSS_SELECTOR, '#root > div > div.model__page > div.card__container > div:nth-child(3) > div > div > div.MuiCardActions-root.MuiCardActions-spacing.css-ny0n9o > a.MuiButtonBase-root.MuiButton-root.MuiButton-text.MuiButton-textPrimary.MuiButton-sizeLarge.MuiButton-textSizeLarge.MuiButton-root.MuiButton-text.MuiButton-textPrimary.MuiButton-sizeLarge.MuiButton-textSizeLarge.css-1enp6xl').click()
		try:
			self.assertEqual("https://www.veteranhaven.me/housing/2", self.driver.current_url)
			print("Passed Housing Instance Card Test \u2705")
		except AssertionError:
			print("Failed Housing Instance Card Test \U0001F645")
			print(self.driver.current_url)
	
	# Tests the learn more button on instance card
	def testEducationInstanceCard(self):
		self.driver.get("https://www.veteranhaven.me/education")
		self.driver.implicitly_wait(10)
		self.driver.find_element(By.XPATH, '//*[@id="root"]/div/div[2]/div[3]/div[2]/div/div/div[3]/a[1]').click()
		try:
			self.assertEqual("https://www.veteranhaven.me/education/1", self.driver.current_url)
			print("Passed Education Instance Card Test \u2705")
		except AssertionError:
			print("Failed Education Instance Card Test \U0001F645")

	# Tests the learn more button on instance card
	def testHealthcareInstanceCard(self):
		self.driver.get("https://www.veteranhaven.me/healthcare")
		self.driver.implicitly_wait(10)
		# self.driver.find_element(By.XPATH, '/html/body/div/div/div[2]/div[3]/div[1]/div/div/div[3]/a[1]').click()
		self.driver.find_element(By.CSS_SELECTOR, '#root > div > div.model__page > div.card__container > div:nth-child(1) > div > div > div.MuiCardActions-root.MuiCardActions-spacing.css-ny0n9o > a.MuiButtonBase-root.MuiButton-root.MuiButton-text.MuiButton-textPrimary.MuiButton-sizeLarge.MuiButton-textSizeLarge.MuiButton-root.MuiButton-text.MuiButton-textPrimary.MuiButton-sizeLarge.MuiButton-textSizeLarge.css-1enp6xl').click()
		try:
			self.assertEqual("https://www.veteranhaven.me/healthcare/0", self.driver.current_url)
			print("Passed Healthcare Instance Card Test \u2705")
		except AssertionError:
			print("Failed Healthcare Instance Card Test \U0001F645")
			print(self.driver.current_url)
	
	# Tests the links in the footer
	def testHousingInstancePage(self):
		self.driver.get("https://www.veteranhaven.me/education/41")
		self.driver.implicitly_wait(10)
		self.driver.find_element(By.XPATH, "//a[@href='/healthcare/9']").click()
		try:
			self.assertEqual("https://www.veteranhaven.me/healthcare/9", self.driver.current_url)
			print("Passed Housing Instance Page Footer Test \u2705")
		except AssertionError:
			print("Failed Housing Instance Page Footer Test \U0001F645")

	# Tests the Pagination
	def testPagination(self):
		self.driver.get("https://www.veteranhaven.me/housing")
		self.driver.implicitly_wait(10)
		self.driver.find_element(By.XPATH, '//*[@id="root"]/div/div[2]/div[4]/div/nav/ul/li[3]/button').click()
		try:
			self.assertEqual("https://www.veteranhaven.me/housing", self.driver.current_url)
			print("Passed Pagination test \u2705")
		except AssertionError:
			print("Failed Pagination test \U0001F645")

	def tearDown(self):
		self.driver.close()

if __name__ == "__main__":
    unittest.main()