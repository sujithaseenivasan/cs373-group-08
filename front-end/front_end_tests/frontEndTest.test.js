import React from 'react';
import { cleanup, render, screen, waitFor } from '@testing-library/react';
import Home from '../src/pages/Home.tsx';
import { BrowserRouter } from 'react-router-dom';
// import About from '../src/pages/About.tsx';
// import Education from '../src/pages/Education.tsx';
// import Housing from '../src/pages/Housing.tsx';
// import HealthCare from '../src/pages/Healthcare.tsx';
// import NavBar from '../src/components/Navbar.tsx'

afterEach(() => {
	cleanup();
})

test('renders Veteran Haven splash page', async () => {
	render(
		<BrowserRouter>
		<Home />
		</BrowserRouter>
	)
	await waitFor(() => {
		const welcomeTitle = screen.getByText('Welcome to Veteran Haven');
		expect(welcomeTitle).toBeInTheDocument();
	});
});

test('renders Veteran Haven splash page', async () => {
	render(
		<BrowserRouter>
		<Home />
		</BrowserRouter>
	)
	await waitFor(() => {
		const welcomeTitle = screen.getByText('What We Do');
		expect(welcomeTitle).toBeInTheDocument();
	});
});

// test('renders About splash page', async () => {
// 	render(<About />);
// 	const about = screen.getByText('about us');
// 	expect(about).toBeInTheDocument();
// });

// test('renders Education splash page', async () => {
// 	render(<Education />);
// 	const edu = screen.getByText('University of Texas at Austin');
// 	expect(edu).toBeInTheDocument();
// });

// test('renders Housing splash page', async () => {
// 	render(<Housing />);
// 	const house = screen.getByText('Caritas of Austin');
// 	expect(house).toBeInTheDocument();
// });

// test('renders HealthCare splash page', async () => {
// 	render(<HealthCare />);
// 	const hc = screen.getByText('Austin Healthcare Facilities for Veterans');
// 	expect(hc).toBeInTheDocument();
// });

// test('renders NavBar', async () => {
// 	render(<NavBar />);
// 	const bar = screen.getByText('About');
// 	expect(bar).toBeInTheDocument();
// });

// test('renders SUJITHA SEENIVASAN in about page', async () => {
// 	render(<About />);
// 	const name = screen.getByText('SUJITHA SEENIVASAN');
// 	expect(name).toBeInTheDocument();
// });

// test('renders RAJVI SHAH in about page', async () => {
// 	render(<About />);
// 	const name = screen.getByText('RAJVI SHAH');
// 	expect(name).toBeInTheDocument();
// });

// test('renders NATHAN HERNANDEZ in about page', async () => {
// 	render(<About />);
// 	const name = screen.getByText('WELCOME TO VETERAN HAVEN');
// 	expect(name).toBeInTheDocument();
// });

// test('renders JACOB LOPEZ in about page', async () => {
// 	render(<About />);
// 	const name = screen.getByText('WELCOME TO VETERAN HAVEN');
// 	expect(name).toBeInTheDocument();
// });

// test('renders CONNOR CHUNG in about page', async () => {
// 	render(<About />);
// 	const name = screen.getByText('CONNOR CHUNG');
// 	expect(name).toBeInTheDocument();
// });