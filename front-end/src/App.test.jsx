import React from 'react';
import { cleanup, render, screen, waitFor } from '@testing-library/react';
import Home from '../src/pages/Home.tsx';
import { BrowserRouter } from 'react-router-dom';
// import About from '../src/pages/About.tsx';
// import Education from '../src/pages/Education.tsx';
// import Housing from '../src/pages/Housing.tsx';
// import HealthCare from '../src/pages/Healthcare.tsx';
import NavBar from '../src/components/Navbar.tsx';
import { Nav } from 'react-bootstrap';

afterEach(() => {
	cleanup();
})

test('renders Veteran Haven splash page', async () => {
	render(
		<BrowserRouter>
		<Home />
		</BrowserRouter>
	)
	await waitFor(() => {
		const welcomeTitle = screen.getByText('welcome to veteran haven');
		expect(welcomeTitle).toBeInTheDocument();
	});
});

test('renders What We Do', async () => {
	render(
		<BrowserRouter>
		<Home />
		</BrowserRouter>
	)
	await waitFor(() => {
		const welcomeTitle = screen.getByText('What We Do');
		expect(welcomeTitle).toBeInTheDocument();
	});
});

test('renders Homeless Population', async () => {
	render(
		<BrowserRouter>
		<Home />
		</BrowserRouter>
	)
	await waitFor(() => {
		const welcomeTitle = screen.getByText('227,590');
		expect(welcomeTitle).toBeInTheDocument();
	});
});

test('renders percentage that experiences', async () => {
	render(
		<BrowserRouter>
		<Home />
		</BrowserRouter>
	)
	await waitFor(() => {
		const welcomeTitle = screen.getByText('4.8%');
		expect(welcomeTitle).toBeInTheDocument();
	});
});

test('renders our mission', async () => {
	render(
		<BrowserRouter>
		<Home />
		</BrowserRouter>
	)
	await waitFor(() => {
		const welcomeTitle = screen.getByText('Our mission is to empower and guide veterans towards a successful and fulfilling post-military life. We are committed to providing comprehensive resources and support, spanning education, employment, housing, and more. We aim to serve those who have served our nation, ensuring they seamlessly transition to civilian life with access to the tools and opportunities necessary for personal and professional growth.');
		expect(welcomeTitle).toBeInTheDocument();
	});
});

test('renders HealthCare to confirm it', async () => {
	render(
		<BrowserRouter>
		<NavBar />
		</BrowserRouter>
	)
	const bar = screen.getByText('Healthcare');
	expect(bar).toBeInTheDocument();
});

test('renders Search to confirm it', async () => {
	render(
		<BrowserRouter>
		<NavBar />
		</BrowserRouter>
	)
	const bar = screen.getByText('Search');
	expect(bar).toBeInTheDocument();
});

test('renders Housing to confirm it', async () => {
	render(
		<BrowserRouter>
		<NavBar />
		</BrowserRouter>
	)
	const bar = screen.getByText('Housing');
	expect(bar).toBeInTheDocument();
});

test('renders Home to confirm it', async () => {
	render(
		<BrowserRouter>
		<NavBar />
		</BrowserRouter>
	)
	const bar = screen.getByText('Home');
	expect(bar).toBeInTheDocument();
});

test('renders Education and Employment to confirm it', async () => {
	render(
		<BrowserRouter>
		<NavBar />
		</BrowserRouter>
	)
	const bar = screen.getByText('Education and Employment');
	expect(bar).toBeInTheDocument();
});