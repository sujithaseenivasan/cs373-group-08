
const fonts = {
    fonts: {
        header1: "'Bodoni Moda', serif",
        subheader: "'Montserrat', sans-serif"
    }
}

export default fonts;