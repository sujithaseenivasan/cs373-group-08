import React, { useState, useEffect } from 'react';
import axios from 'axios';
import HealthcareCard from '../components/HealthcareCard';
import Typography from '@mui/material/Typography';
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';
// import CircularProgress from '@mui/material/CircularProgress';
import '../styles/Card.css';
import theme from '../styles/theme';
import fonts from '../styles/fonts';
import { text } from '../data/Healthcare_data';
import DropDownMenu from '../components/DropDownMenu';
import SortDropDown from '../components/SortDropDown';

export interface HealthcareData {
  address: string;
  assistance_type: string;
  city_id: number;
  description: string;
  facility_type: string;
  foreign_id: number;
  img_src: string;
  instance: number;
  latitude: number;
  longitude: number;
  name: string;
  phone: string;
  vid_src: string;
  website: string;
  related_housing_id: number;
  related_education_id: number;
}
const baseURL = 'https://api.veteranhaven.me/healthcare';
const citiesURL = 'https://api.veteranhaven.me/cities';
// const paginationURL = "https://api.veteranhaven.me/healthcare_page";

// const baseURL = 'http://127.0.0.1:5009/healthcare';
// const citiesURL = 'http://127.0.0.1:5009/cities'; // Adjust the URL accordingly

function Healthcare() {
  const [healthcareData, setHealthcareData] = useState<HealthcareData[]>([]);
  const [sortedData, setSortedData] = useState<HealthcareData[]>([]);
  const [pageData, setPageData] = useState<HealthcareData[]>([]);
  const [page, setPage] = React.useState(1);
  const [ascendingOrder, setAscendingOrder] = useState(true); // Default to ascending order
  const [filterOptions, setFilterOptions] = useState<{ label: string; items: string[] }[]>([
  ]);
  const [masterFilterList, setMasterFilters] = useState<{ [key: string]: string[] }>({}); // State to hold selected filters

  const [city_list, setCityList] = useState<string[]>([]); // State to hold city list
  // Function to fetch cities
  const get_cities = async () => {
    try {
      const response = await axios.get(citiesURL);
      const cities = response.data['city'];
      setCityList(cities);
      console.log(city_list)
    } catch (error) {
      console.error('Error fetching cities:', error);
    }
  };

  const fillFilterOptions = () => {
    const normalizeType = (type: string): string => {
      // Normalize the string: convert to lowercase and remove trailing whitespace
      return type.toUpperCase().trim();
    };
    const uniqueCities = Array.from(new Set(healthcareData.map(item => city_list[item.city_id]))).sort();
    const uniqueFacilityTypes = Array.from(new Set(healthcareData.map(item => normalizeType(item.facility_type))));
    const uniqueSupportTypes = Array.from(new Set(healthcareData.map(item =>  normalizeType(item.assistance_type))));
    const uniqueResourceTypes = Array.from(new Set(healthcareData.map(item => item.phone !== 'N/A' ? "Yes" : "No") ));
      setFilterOptions([
      { label: 'Cities', items: uniqueCities },
      { label: 'Facility Type', items: uniqueFacilityTypes },
      { label: 'Type of support', items: uniqueSupportTypes },
      { label: 'Has contact', items: uniqueResourceTypes }
      
    ]);
  };
  const handleFilterSelection = async (label: string, selectedFilters: string[]) => {
    try {
      console.log(label, selectedFilters);
      setMasterFilters(prevFilters => ({
        ...prevFilters, // Spread the previous state
        [label]: selectedFilters // Add new key-value pair
      }));
    } catch (error) {
      console.error('Error filtering data:', error);
    }
  };
  useEffect(() => {
    const fetchData = async () => {
      try {
        console.log('Making Axios call with masterFilterList:', masterFilterList);
        const response = await axios.post(baseURL + '/filter/sort/', { masterFilterList, ascendingOrder});
        
        setSortedData(response.data?.filter || []);
        setPage(1);
      } catch (error) {
        console.error('Error filtering data:', error);
      }
    };

    fetchData(); // Call fetchData function when masterFilterList changes
  }, [masterFilterList]); // Watching for changes in masterFilterList


  const handleSortChange = () => {
    setAscendingOrder(prevOrder => !prevOrder); // Toggle between true and false
    // Sort the data alphabetically by name
    const sorted = [...sortedData].sort((a, b) => {
      if (!ascendingOrder) {
        return a.name.localeCompare(b.name);
      } else {
        return b.name.localeCompare(a.name);
      }
    });
    setSortedData(sorted);
  };


  useEffect(() => {
    fetchData();
    get_cities();
  }, []);

  useEffect(() => {
    fillFilterOptions();
  }, [healthcareData, city_list]);

  const fetchData = async () => {
    try {
      const response = await axios.get(baseURL);
      const instances = response.data?.healthcare || [];
      setHealthcareData(instances);
      setSortedData(instances);
    } catch (error) {
      console.error('Error fetching healthcare data:', error);
    }
  };

  const handlePageChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setPage(value);
  };

  useEffect(() => {
    const startIndex = (page - 1) * 9;
    const endIndex = startIndex + 9;
    setPageData(sortedData.slice(startIndex, endIndex));
  }, [sortedData, page]);

  // const handleRefresh = () => {
  //   window.location.reload();
  // };

  return (
    <div className="model__page">
      <h1 className="healthcare__header"  style={{ fontFamily: fonts.fonts.header1 , color: theme.colors.primary_green, fontStyle: "italic", marginLeft: 20, marginRight: 20}}>Healthcare</h1>
      <p className="healthcare__text" style={{ fontFamily: fonts.fonts.subheader , color: theme.colors.background_brown2, marginLeft: 20, marginRight: 20}}>{text[0].description}</p>
      <p className="healthcare__text" style={{ fontFamily: fonts.fonts.subheader , color: theme.colors.background_brown2}}>&nbsp;</p>
      <div className="sort__container" style={{fontFamily: fonts.fonts.subheader, color: theme.colors.background_brown2, fontWeight: 900, marginLeft: 20, marginRight: 20}}> Total Instances: {sortedData.length}</div>

      <div className="filter-container" style={{ display: 'flex', flexDirection: 'row',  justifyContent: 'center', width: '80%', margin: '0 auto'}}>
      <SortDropDown onSortChange={handleSortChange} />
        {filterOptions.map((options, index) => (
          <DropDownMenu key={index} options={options} onFilterSelection={handleFilterSelection}/>
        ))}
      </div>

      <div className="card__container">
        {pageData.map((instance, index) => (
          <div key={index}>
          <HealthcareCard key={index} id={instance.instance} searchTerm={''} {...instance} />
          </div>
        ))}
      </div>
      <div className="pagination__container">
      <Stack spacing={2}>
        <Typography variant="h4" fontFamily="Poppins"
        >Page: {page}</Typography>
        <Pagination
          count={Math.ceil(sortedData.length / 9)}
          page={page}
          onChange={handlePageChange}
          sx={{
            '& .MuiPaginationItem-root': {
              fontSize: '1.5rem', 
              minWidth: '30px', 
              minHeight: '30px', 
              margin: '0 5px', 
            },
            '& .MuiPaginationItem-ellipsis': {
              fontSize: '2rem', 
            },
          }}
        />
      </Stack>
      </div>
    </div>
  );
}

export default Healthcare;
