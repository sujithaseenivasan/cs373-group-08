export const text = [
  {
    description:
      'The purpose of this model is to bridge veterans to higher education and the workforce. It serves as a direct link between veterans and educational or employment opportunities tailored to their unique skills and experiences, smoothing their reintegration into civilian life. \n',
  }
];