export const text = [
  {
    description:
      'This model focuses on addressing the housing needs of veterans by providing them a centralized platform that has a list of housing facilities that are designed for veterans. The purpose is to simplify the search process, ensuring that they find safe, accessible, and supportive housing solutions as they transition back to civilian life. \n',
  }
];