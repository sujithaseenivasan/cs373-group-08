export const text = [
  {
    description:
      'This model is dedicated to improving veteran’s access to healthcare. It provides a list of clinics and health services with an emphasis on addressing the specific health challenges faced by veterans. The purpose is to facilitate easier access to mental, physical, and emotional health support tailored to the veteran community. \n',
  }
];