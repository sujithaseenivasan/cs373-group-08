# Makefile for managing Docker-related tasks

# Define variables
DOCKER_COMPOSE = docker-compose

# Targets and Recipes

# Target: test
# Description: Run a test command
test:
	@echo "Running test command..."
	@echo "test"

# Target: version
# Description: Display Docker version information
version:
	@echo "Displaying versions..."
	@docker --version
	@$(DOCKER_COMPOSE) --version
	@echo -n "node "
	@node --version 
	@echo -n "yarn "
	@yarn --version yarn

backend-build:
	docker-compose build backend

backend-run:
	docker-compose up backend

frontend-build:
	docker-compose build frontend

frontend-run:
	docker-compose up frontend

# Description: format all files using Prettier
format:
	@echo "Formatting files..."
	cd front-end; \
	yarn format;
	cd ..;

# Target: stop
# Description: Stop Docker containers using docker-compose
stop:
	@echo "Stopping Docker containers..."
	$(DOCKER_COMPOSE) down

# Target: clean
# Description: Clean up Docker artifacts
clean: stop
	@echo "Cleaning up Docker artifacts..."
	# Add additional cleanup steps if needed'

