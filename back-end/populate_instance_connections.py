from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from endpoints import Health, House, Education
import random


DATABASE_URL = "mysql+pymysql://admin:Veteranhaven373@veteran-haven-db-1.cn6akukgs0pd.us-east-2.rds.amazonaws.com:3306/veteran_haven_db"
    

def update_health_connections():
    engine = create_engine(DATABASE_URL)
    Session = sessionmaker(bind=engine)
    session = Session()

    #health_instances = session.query(Health).filter((Health.related_education_id == None) | (Health.related_housing_id == None)).all()
    health_instances = session.query(Health).all()
    for health_instance in health_instances:
        education_instances = session.query(Education).filter(Education.city_id == health_instance.city_id).all()
        housing_instances = session.query(House).filter(House.city_id == health_instance.city_id).all()
        if education_instances and housing_instances:
            random_education_instance = random.choice(education_instances)
            random_housing_instance = random.choice(housing_instances)

            health_instance.related_education_id = random_education_instance.instance
            health_instance.related_housing_id = random_housing_instance.instance
        else:
            health_instance.related_education_id = 0
            health_instance.related_housing_id = 0

        print("Updated instance ", health_instance.instance)

    # Commit the changes to the database
    session.commit()


def update_housing_connections():
    engine = create_engine(DATABASE_URL)
    Session = sessionmaker(bind=engine)
    session = Session()

    #housing_instances = session.query(House).filter((House.related_education_id == None) | (House.related_healthcare_id == None)).all()
    housing_instances = session.query(House).all()

    for housing_instance in housing_instances:   
        education_instances = session.query(Education).filter(Education.city_id == housing_instance.city_id).all()
        healthcare_instances = session.query(Health).filter(Health.city_id == housing_instance.city_id).all()

        if education_instances and healthcare_instances:
            random_education_instance = random.choice(education_instances)
            random_healthcare_instance = random.choice(healthcare_instances)
            
            housing_instance.related_education_id = random_education_instance.instance
            housing_instance.related_healthcare_id = random_healthcare_instance.instance
        else:
            housing_instance.related_education_id = 0
            housing_instance.related_healthcare_id = 0

        print("Updated instance ", housing_instance.instance)
            
    session.commit()


def update_education_connections():
    engine = create_engine(DATABASE_URL)
    Session = sessionmaker(bind=engine)
    session = Session()

    #education_instances = session.query(Education).filter((Education.related_housing_id == None) | (Education.related_healthcare_id == None)).all()
    education_instances = session.query(Education).all()

    for education_instance in education_instances:   
        housing_instances = session.query(House).filter(House.city_id == education_instance.city_id).all()
        healthcare_instances = session.query(Health).filter(Health.city_id == education_instance.city_id).all()

        if healthcare_instances and housing_instances:
            random_housing_instance = random.choice(housing_instances)
            random_healthcare_instance = random.choice(healthcare_instances)
            
            education_instance.related_housing_id = random_housing_instance.instance
            education_instance.related_healthcare_id = random_healthcare_instance.instance
        else:
            education_instance.related_housing_id = 0
            education_instance.related_healthcare_id = 0

        print("Updated instance ", education_instance.instance)
            
    session.commit()

def update_all():
    
    update_education_connections()
    
    update_health_connections()

    update_housing_connections()

update_all()