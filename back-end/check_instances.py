from endpoints import city_list, DBsession, House, Health, Education


def check_instances(check : int):
    ret_list = []
    session = DBsession()
    
    for key in city_list.keys():
        count = 0
        h = session.query(House).filter_by(city_id = key).all()
        if h:
            count += 1
        hc = session.query(Health).filter_by(city_id = key).all()
        if hc:
            count += 1
        e = session.query(Education).filter_by(city_id = key).all()
        if e:
            count += 1
        
        if count == check:
            ret_list.append(key)
        
    session.close()
    print(ret_list)
    print("----- counter ------")
    print(len(ret_list))
        
        
if __name__ == "__main__":
    print("--------------------------- STARTING CHECK for 3------------------------------------------------")
    check_instances(3)
    print("--------------------------- FINISHING CHECK------------------------------------------------")
    print("--------------------------- STARTING CHECK for 2------------------------------------------------")
    check_instances(2)
    print("--------------------------- FINISHING CHECK------------------------------------------------")
    print("--------------------------- STARTING CHECK for 1------------------------------------------------")
    check_instances(1)
    print("--------------------------- FINISHING CHECK------------------------------------------------")