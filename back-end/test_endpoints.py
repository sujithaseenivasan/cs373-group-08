import unittest
from endpoints import app  

class TestEndpoints(unittest.TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        self.app = app.test_client()

    def assertJsonResponse(self, response):
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content_type, 'application/json')
        self.assertTrue(response.is_json)
    
    # def test_housing_page(self):
    #     response = self.app.get('/housing')
    #     self.assertEqual(response.status_code, 200)

    # def test_housing_pagination(self):
    #     response = self.app.get('/housing_page?page=3')
    #     self.assertEqual(response.status_code, 200)
    
    # def test_housing_instances(self):
    #     response = self.app.get('/housing/1')  
    #     self.assertEqual(response.status_code, 200)
    
    # def test_education_page(self):
    #     response = self.app.get('/education')
    #     self.assertEqual(response.status_code, 200)
    
    # def test_education_pagination(self):
    #     response = self.app.get('/education_page?page=9')
    #     self.assertEqual(response.status_code, 200)

    # def test_education_instances(self):
    #     response = self.app.get('/education/3')
    #     self.assertEqual(response.status_code, 200)
    
    # def test_health_page(self):
    #     response = self.app.get('/healthcare')
    #     self.assertEqual(response.status_code, 200)
    
    # def test_health_pagination(self):
    #     response = self.app.get('/healthcare_page?page=5')
    #     self.assertEqual(response.status_code, 200)
    
    # def test_health_instances(self):
    #     response = self.app.get('/healthcare/60')  
    #     self.assertEqual(response.status_code, 200)
    
    # def test_housing_page_format(self):
    #     response = self.app.get('/housing')
    #     self.assertJsonResponse(response)

    # def test_housing_pagination_format(self):
    #     response = self.app.get('/housing_page?page=3')
    #     self.assertJsonResponse(response)
    
    # def test_housing_instances_format(self):
    #     response = self.app.get('/housing/1')  
    #     self.assertJsonResponse(response)
    
    # def test_education_page_format(self):
    #     response = self.app.get('/education')
    #     self.assertJsonResponse(response)
    
    # def test_education_pagination_format(self):
    #     response = self.app.get('/education_page?page=9')
    #     self.assertJsonResponse(response)

    # def test_education_instances_format(self):
    #     response = self.app.get('/education/3')
    #     self.assertJsonResponse(response)
    
    # def test_health_page_format(self):
    #     response = self.app.get('/healthcare')
    #     self.assertJsonResponse(response)
    
    # def test_health_pagination_format(self):
    #     response = self.app.get('/healthcare_page?page=5')
    #     self.assertJsonResponse(response)
    
    # def test_health_instances_format(self):
    #     response = self.app.get('/healthcare/60')  
    #     self.assertJsonResponse(response)
        
    def test_education(self):
        # Prepare test data
        test_data = {
            "masterFilterList": {
                "Funding for": [">1000"]
            },
            "ascendingOrder": True
        }
        # Make request
        response = self.app.post('/education/filter/sort/', json=test_data)
        
        self.assertEqual(response.status_code, 200)

    def test_healthcare(self):
        # Prepare test data
        test_data = {
            "masterFilterList": {
                "Facility Type": ["GENERAL HEALTH CARE"]
            },
            "ascendingOrder": True
        }
        # Make request
        response = self.app.post('/healthcare/filter/sort/', json=test_data)
        
        self.assertEqual(response.status_code, 200)

    def test_housing(self):
        # Prepare test data
        test_data = {
            "masterFilterList": {
                "Eligibility": ["OTHER"]
            },
            "ascendingOrder": True
        }
        # Make request
        response = self.app.post('/housing/filter/sort/', json=test_data)
        
        self.assertEqual(response.status_code, 200)
        
    # def test_sort_degree_1(self):
    #     # Prepare test data
    #     test_data = {
    #         "masterFilterList": {
    #             "Funding for": [">1000"]
    #         },
    #     }
    #     # Make request
    #     response = self.app.post('/filter/sort/', json=test_data)
        
    #     self.assertEqual(response.status_code, 200)

    # def test_sort_degree_2(self):
    #     # Prepare test data
    #     test_data = {
    #         "masterFilterList": {
    #             "Cities": ["Austin", "Dallas"]
    #             "Funding for": [">1000"]
    #         },
    #     }
    #     # Make request
    #     response = self.app.post('/filter/sort/', json=test_data)
        
    #     self.assertEqual(response.status_code, 200)

    # def test_sort_degree_3(self):
    #     # Prepare test data
    #     test_data = {
    #         "masterFilterList": {
    #             "Degree Type": ["All"]
    #             "Cities": ["Austin", "Dallas"]
    #             "Funding for": [">1000"]
    #         },
    #     }
    #     # Make request
    #     response = self.app.post('/filter/sort/', json=test_data)
        
    #     self.assertEqual(response.status_code, 200)

if __name__ == '__main__':
    unittest.main()
