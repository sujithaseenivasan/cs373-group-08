from flask import Flask, Response, request
from flask_cors import CORS
from sqlalchemy import create_engine, Column, Integer, String, ForeignKey
from flask import session
from sqlalchemy.orm import sessionmaker, declarative_base
from sqlalchemy.orm import class_mapper as class_mapper

# for searching
from fuzzywuzzy import fuzz


import requests
import json
from sqlalchemy.orm import relationship
from sqlalchemy import desc, asc
from sqlalchemy import not_
from sqlalchemy import func
from sqlalchemy import cast, Integer


###########################################################################
# from password import api_key
import re

url = 'https://sandbox-api.va.gov/services/va_facilities/v1/facilities'
# Headers including the API key
headers = {
    'apikey': "",
}

params = {
    'state': 'TX',
    'per_page': 10,
}

city_list = {
    1: "Austin",
    2: "Dallas",
    3: "Houston",
    4: "San Antonio",
    5: "Fort Worth",
    6: "El Paso",
    7: "Arlington",
    8: "Corpus Christi",
    9: "Plano",
    10: "Lubbock",
    11: "Laredo",
    12: "Irving",
    13: "Garland",
    14: "Frisco",
    15: "Amarillo",
    16: "Grand Prairie",
    17: "McKinney",
    18: "Brownsville",
    19: "Killeen",
    20: "Pasadena",
    21: "Mesquite city",
    22: "McAllen",
    23: "Denton",
    24: "Waco",
    25: "Midland",
    26: "Carrollton",
    27: "Abilene",
    28: "Lewisville",
    29: "Pearland",
    30: "Round Rock",
    31: "Kerrville",
    32: "Mission",
    33: "Fort Cavazos",
    34: "Sheppard AFB",
    35: "Temple",
    36: "College Station",
    37: "Tyler",
    38: "Hurst",
    39: "Texarkana",
    40: "Huntsville",
    41: "Westworth Village",
    42: "Harker Heights",
    43: "Pantego",
    44: "Beaumont",
    45: "Childress",
    46: "Dalhart",
    47: "Big Spring",
    48: "Odessa",
    49: "Fort Stockton",
    50: "San Angelo",
    51: "Bonham",
    52: "Decatur",
    53: "Granbury",
    54: "Greenville",
    55: "Sherman",
    56: "Lufkin",
    57: "Galveston",
    58: "Conroe",
    59: "Katy",
    60: "Lake Jackson",
    61: "Richmond",
    62: "Tomball",
    63: "Texas City",
    64: "Humble",
    65: "Sugar Land",
    66: "Wichita Falls",
    67: "Longview",
    68: "Victoria",
    69: "New Braunfels",
    70: "Palestine",
    71: "Brownwood",
    72: "Cedar Park",
    73: "Copperas Cove",
    74: "LaGrange",
    75: "Harlingen",
    76: "Pleasant",
    77: "Taft",
    78: "Dayton",
    79: "Pass",
    80: "Coolidge",
    81: "Madisonville",
    82: "Lockhart",
    83: "Aspermont",
    84: "Vernon",
    85: "Knox City",
    86: "Boston",
    87: "Marfa",
    88: "Bartlett",
    89: "Cleveland",
    90: "Asherton",
    91: "Meridian",
    92: "Palacios",
    93: "Taylor",
    94: "Groesbeck",
    95: "Newcastle",
    96: "Gregory",
    97: "Franklin",
    98: "Arthur",
    99: "Smithville",
    100: "Rivers",
    101: "Livingston",
    102: "Centerville",
    103: "Caldwell",
    104: "Cuero",
    105: "Como",
    106: "Mcallen",
    107: "Hidalgo",
    108: "Paris",
    109: "Llano",
    110: "Munday",
    111: "Hearne",
    112: "Grange",
    113: "Quanah",
    114: "Robstown",
    115: "Memphis",
    116: "Talco",
    117: "Elgin",
    118: "Sinton",
    119: "Maud",
    120: "Thorndale",
    121: "Detroit",
    122: "Garrison",
    123: "Deport",
    124: "Edcouch",
    125: "Bridgeport",
    126: "Mercedes",
    127: "Rotan",
    128: "Kalb",
    129: "Benito",
    130: "Merkel",
    131: "Crystal City",
    132: "Tulia",
    133: "Olney",
    134: "Angleton",
    135: "Crowell",
    136: "Diego",
    137: "Anson",
    138: "Star",
    139: "Spearman",
    140: "Bryan",
    141: "Schulenburg",
    142: "Cooper",
    143: "Timpson",
    144: "Fabens",
    145: "Juan",
    146: "Pharr",
    147: "Donna",
    148: "Grandview",
    149: "Daingerfield",
    150: "Liberty",
    151: "Howe",
    152: "Flatonia",
    153: "Throckmorton",
    154: "Grandfalls",
    155: "Paducah",
    156: "Brenham",
    157: "Haskell",
    158: "Marcos",
    159: "Archer City",
    160: "Seymour",
    161: "Teague",
    162: "Burnet",
    163: "Mathis",
    164: "Atlanta",
    165: "Rosebud",
    166: "Edinburg",
    167: "Colorado City",
    168: "Granger",
    169: "Clarksville",
    170: "Stamford",
    171: "Borger",
    172: "Floresville",
    173: "Grande City",
    174: "Alice",
    175: "Clifton",
    176: "Marlin",
    177: "Blossom",
    178: "Linden",
    179: "Isabel",
    180: "Rogers",
    181: "Elsa",
    182: "Omaha",
    183: "Roma",
    184: "Springs",
    185: "Alamo",
    186: "Bellville",
    187: "Cleburne",
    188: "Hallettsville",
    189: "Oglesby",
    190: "Wake Village",
    191: "Marble Falls",
    192: "Dublin",
    193: "Campo",
    194: "Falfurrias",
    195: "Hillsboro",
    196: "Joya",
    197: "Nixon",
    198: "Weslaco",
    199: "Leon",
    200: "McLean",
    201: "Canyon",
    202: "Electra",
    203: "Baytown",
    204: "Naples",
    205: "Whitney",
    206: "Lott",
    207: "Clarendon",
    208: "Johnson City",
    209: "Anna",
    210: "Orange",
    211: "Avery",
    212: "Wortham",
    213: "Mexia",
    214: "Rock",
    215: "Bastrop",
    216: "Moody",
    217: "Avinger",
    218: "Fresnos",
    219: "Cumby",
    220: "Beeville",
    221: "Bryson",
    222: "Georgetown",
    223: "Plains",
    224: "Stockdale",
    225: "Weatherford",
    226: "Nocona",
    227: "Feria",
    228: "Ingleside",
    229: "Wells",
    230: "Gorman",
    231: "Hebbronville",
    232: "Loraine",
    233: "Kyle",
    234: "Alpine",
    235: "Luling",
    236: "Henrietta",
    237: "Burkburnett"
}
###########################################################################

app = Flask(__name__)
CORS(app)
Base = declarative_base()
DATABASE_URL = "mysql+pymysql://admin:Veteranhaven373@veteran-haven-db-1.cn6akukgs0pd.us-east-2.rds.amazonaws.com:3306/"
engine = create_engine(DATABASE_URL, pool_size=20, max_overflow=10, pool_timeout=30)

DBsession = sessionmaker(bind=engine)

## class
class House(Base):
    __tablename__ = "House"
    __table_args__ = {'schema': 'veteran_haven_db'}
    instance = Column(Integer, primary_key=True, autoincrement=True)
    city_id = Column(Integer)
    
    name = Column(String(255))
    address = Column(String(255))
    phone = Column(String(255))
    hours = Column(String(255))
    eligibility_requirements = Column(String(255))
    description = Column(String(255))
    area_served = Column(String(255))
    link = Column(String(255))
     
    img_src = Column(String(255))
    vid_src = Column(String(255))
    latitude = Column(String(20))
    longitude = Column(String(20))
    related_healthcare_id = Column(Integer)
    related_education_id = Column(Integer)
    
    foreign_id = Column(Integer, ForeignKey('veteran_haven_db.texas_cities.id'))
    city = relationship("City")
    
    def as_dict(self):
        fields = {}
        for c in self.__table__.columns:
            fields[c.name] = getattr(self, c.name)
        return fields
        
class Health(Base):
    __tablename__ = "Health"
    __table_args__ = {'schema': 'veteran_haven_db'}
    instance = Column(Integer, primary_key=True, autoincrement=True)
    city_id = Column(Integer)
    name = Column(String(255))
    description = Column(String(255))
    address = Column(String(255))
    assistance_type = Column(String(255))
    # additional_fee = Column(Integer)
    facility_type = Column(String(255))
    website = Column(String(255))
    phone = Column(String(255))
    img_src = Column(String(255))
    vid_src = Column(String(255))
    latitude = Column(String(20))
    longitude = Column(String(20))
    related_housing_id = Column(Integer)
    related_education_id = Column(Integer)
    
    foreign_id = Column(Integer, ForeignKey('veteran_haven_db.texas_cities.id'))
    city = relationship("City")
    

    def as_dict(self):
        fields = {}
        for c in self.__table__.columns:
            fields[c.name] = getattr(self, c.name)
        return fields
        
class Education(Base):
    __tablename__ = "Education"
    __table_args__ = {'schema': 'veteran_haven_db'}
    instance = Column(Integer, primary_key=True, autoincrement=True)
    city_id = Column(Integer)
    name = Column(String(255))
    address = Column(String(255))
    contribution = Column(String(255))
    distance_learning = Column(String(255))
    degree = Column(String(255))
    funding = Column(String(255))
    link = Column(String(255))
    img_src = Column(String(255))
    vid_src = Column(String(255))
    latitude = Column(String(20))
    longitude = Column(String(20))
    related_healthcare_id = Column(Integer)
    related_housing_id = Column(Integer)
    
    foreign_id = Column(Integer, ForeignKey('veteran_haven_db.texas_cities.id'))
    city = relationship("City")
    
    def as_dict(self):
        fields = {}
        for c in self.__table__.columns:
            fields[c.name] = getattr(self, c.name)
        return fields
    
class City(Base):
    __tablename__ = "texas_cities"
    __table_args__ = {'schema': 'veteran_haven_db'}
    id = Column(Integer, primary_key=True)
    city_name = Column(String(45))
    
    def as_dict(self):
        fields = {}
        for c in self.__table__.columns:
            fields[c.name] = getattr(self, c.name)
        return fields
    
    
def commit(obj):
    session = DBsession()
    try:
        session.add(obj)
        session.commit()
    except Exception as e:
        session.rollback()
        print(f"Error committing to the database: {e}")
    finally:
        session.close()
        
# INSERT INTO table_name (column1, column2, column3, ...)
# VALUES (value1, value2, value3, ...);


##########################################################################################################################################
# DEFAULT METHODS
##########################################################################################################################################

def default_health():
    session = DBsession()
    existing_instance = session.query(Health).filter(Health.city_id == 0).first()
    if existing_instance:
        return
    instance = Health(
        city_id = 0,
        name = "VA Health Care",
        description = "With VA health care, you’re covered"
            + "for regular checkups with your primary care provider and appointments with specialists (like cardiologists, gynecologists, and mental health providers)."
            + "You can access Veterans health care services like home health and geriatric (elder) care, and you can get medical equipment, prosthetics, and prescriptions. Find out how to apply for and manage the health care benefits you've earned.",
        address = "online",
        assistance_type = "like cardiologists, gynecologists, and mental health providers",
        facility_type = "General Health Care",
        website = "https://www.va.gov/health-care/",
        phone = "877-327-0022"
    )
    commit(instance)

def default_house():
    session = DBsession()
    existing_instance = session.query(House).filter(House.city_id == 0).first()
    if existing_instance:
        return
    instance = House(
        city_id = 0,
        name = "VA Housing Assistance",
        address = "online",
        phone = "800-827-1000",
        hours = "24/7",
        eligibility_requirements = "Veteran",
        description = "VA housing assistance can help Veterans," +
        "service members and their surviving spouses to buy a home" + 
        " or refinance a loan. We also offer benefits and services to " + 
        "help you build, improve, or keep your current home. Find out how" + 
        "to apply for and manage the Veterans housing assistance benefits you’ve earned.",
        area_served = "United States",
        link = "https://www.va.gov/housing-assistance/"
    )
    commit(instance)


def default_education(): 
    session = DBsession()
    existing_instance = session.query(Education).filter(Education.city_id == 0).first()
    if existing_instance:
        return
    instance = Education(
        city_id = 0,
        name = "VA Education and Training Benefits",
        address = "Online",
        contribution = "Hazelwood Act / GI Bill",
        distance_learning = "Varies",
        degree = "Varies",
        funding = "GI Bill (888-442-4551)",
        link = "https://www.va.gov/education/"
    )
    commit(instance)
    

##########################################################################################################################################
# HOUSING DATASCRAPE
##########################################################################################################################################

def get_housing_data(file_path):
    housing_data = []
    
    with open(file_path, 'r') as file:
        lines = file.readlines()
        institution_data = {}
        
        for line in lines:
            if line.strip() == '':
                if institution_data:
                    housing_data.append(institution_data)
                    institution_data = {}
            else:
                # print(line)
                key, value = map(str.strip, line.split(':', 1))
                institution_data[key] = value
    return housing_data

def housing():
    file_path = file_path = app.root_path + '/scraped_data/housing.txt'
    house_data = get_housing_data(file_path)
    
    for data in house_data:
        city = data.get('City', '')
        if city not in city_list.values():
            continue
        
        key = 0
        for keys, value in city_list.items():
            if value == city:
                key = keys
        
        name = data.get('Name', '')
        address = data.get('Address', '')
        phone = data.get('Phone', '')
        hours_of_operation = data.get('Hours of Operation', '')
        eligibility_requirements = data.get('Eligibility Requirements', '')
        description = data.get('Description', '')
        area_served = data.get('Area Served', '')
        link = data.get('Link', '')
        
        session = DBsession()
        existing_instance = session.query(House).filter(
            House.address == address,
            ).first()
        
        if existing_instance is None:
            instance = House(
                name = name,
                address = address,
                phone = phone,
                hours = hours_of_operation,
                eligibility_requirements = eligibility_requirements,
                description = description,
                area_served = area_served,
                link = link,
                city_id = key
            )
            commit(instance)
            # print(f"Instance was created: {instance.as_dict()}")
        else:
            print(f"Instance already exists: {existing_instance.as_dict()}")
    default_house()
    return "Housing data has been loaded."

##########################################################################################################################################
# EDUCATION DATASCRAPE
##########################################################################################################################################

def get_education_data(file_path):
    education_data = []
    with open(file_path, 'r') as file:
        lines = file.readlines()
        institution_data = {}
        
        for line in lines:
            if line.strip() == '---------------------------------------------------':
                if institution_data:
                    education_data.append(institution_data)
                    institution_data = {}
            else:
                key, value = map(str.strip, line.split(':', 1))
                institution_data[key] = value
    return education_data

def education():
    file_path = file_path = app.root_path + '/scraped_data/education.txt'
    education_data = get_education_data(file_path)
    

    for data in education_data:
        city = data.get('City', '')
        if city not in city_list.values():
            continue
        
        key = 0
        for keys, value in city_list.items():
            if value == city:
                key = keys
            
        
        name = data.get('Name of the institution', '')
        address = data.get('Address', '')
        contribution = data.get('Contribution Amount', '')
        distance_learning = data.get('Distance Learning Available', '')
        degree = data.get('Degree Level','')
        funding = data.get('Funding available for how many students', '')
        link = data.get('Student veteran link', '')
        session = DBsession()
        existing_instance = session.query(Education).filter(
            Education.name == name,
            Education.address == address,
            ).first()
        
        if existing_instance is None:
            instance = Education(
                name = name,
                city_id = key,
                address = address,
                contribution = contribution,
                distance_learning = distance_learning,
                degree = degree,
                funding = funding,
                link = link
            )
            commit(instance)
            # print(f"Instance was created: {instance.as_dict()}")
        else:
            print(f"Instance already exists: {existing_instance.as_dict()}")
    default_education()
    return "Education data has been loaded."


##########################################################################################################################################
# HEALTH API
##########################################################################################################################################

def get_healthcare_data(file_path):
    healthcare_data = []
    with open(file_path, 'r') as file:
        lines = file.readlines()
        institution_data = {}
        
        for line in lines:
            if line.strip() == '-------------------------------':
                if institution_data:
                    healthcare_data.append(institution_data)
                    institution_data = {}
            else:
                key, value = map(str.strip, line.split(':', 1))
                institution_data[key] = value
    return healthcare_data

def healthcare():
    file_path = file_path = app.root_path + '/scraped_data/healthcare.txt'
    healthcare_data = get_healthcare_data(file_path)
    
    for data in healthcare_data:
        city = data.get('City', '')
        if city not in city_list.values():
            continue
        
        key = 0
        for keys, value in city_list.items():
            if value == city:
                key = keys
                
        name = data.get('Name', '')
        description = data.get('Services', '')
        address = data.get('Address', '')
        assistance_type = data.get('Classification', '')
        facility_type = data.get('Type', '')
        website = data.get('Website', '')
        phone = data.get('Phone', '')
       
        session = DBsession()
        existing_instance = session.query(Health).filter(
            # Health.name == name,
            Health.address == address,
            Health.assistance_type == assistance_type
            ).first()
        
        if existing_instance is None:
            instance = Health(
                city_id = key,
                name = name,
                description = description,
                address = address,
                assistance_type = assistance_type,
                facility_type = facility_type,
                website = website,
                phone = phone
            )
            commit(instance)
            # print(f"Instance was created: {instance.as_dict()}")
        else:
            print(f"Instance already exists: {existing_instance.as_dict()}")
    
    default_health()
    return " BIG DATA has been updated"

##########################################################################################################################################
# REST API CALLS? FROM AXIOS V need to change from SQLAchelemy to Axios
##########################################################################################################################################
@app.before_first_request
def make_database():
    Base.metadata.create_all(engine)
    
@app.route("/")
def main():
    status = ("Welcome to the backend!!!!\n type in /house_load, /l_edu, or /health_load into header to load the api")
    return Response(status, status=200)

@app.route("/housing_page")
def housing_pagination():
    ret_obj = {}
    page_number = request.args.get('page', default=1, type=int)  
    items_per_page = 9
    session = DBsession()
   
    start_index = (page_number - 1) * items_per_page
    end_index = start_index + items_per_page

    instances = session.query(House).slice(start_index, end_index)  
    housing_list = [instance.as_dict() for instance in instances]

    ret_obj['housing'] = housing_list
    session.close()
    return ret_obj

@app.route("/education_page")
def education_pagination():
    ret_obj = {}
    page_number = request.args.get('page', default=1, type=int)  
    items_per_page = 9
    session = DBsession()
   
    start_index = (page_number - 1) * items_per_page
    end_index = start_index + items_per_page

    instances = session.query(Education).slice(start_index, end_index)  
    ed_list = [instance.as_dict() for instance in instances]

    ret_obj['education'] = ed_list
    session.close()
    return ret_obj


@app.route("/healthcare_page")
def health_pagination():
    ret_obj = {}
    page_number = request.args.get('page', default=1, type=int)  
    items_per_page = 9
    session = DBsession()
   
    start_index = (page_number - 1) * items_per_page
    end_index = start_index + items_per_page

    instances = session.query(Health).slice(start_index, end_index)  
    health_list = [instance.as_dict() for instance in instances]

    ret_obj['health'] = health_list
    session.close()
    return ret_obj
    
##########################################################################################################################################
#############################################PROJECT 4###########################PROJECT 4###############PROJECT 4########################
##### PLUS REFACTORED PROJECT 3 CODE

def check_model(model):
    model_class = None
    if model == 'education':
        model_class = Education
    elif model == 'healthcare':
        model_class = Health
    elif model == 'housing':
        model_class = House
        
    if model == 'favicon.ico':
        print("Found unknown file (Skipping it)")
        return None

    if not model_class:
        return {"error": f"Model '{model}' not found"}
    
    return model_class

@app.route("/load/<param>")
def load(param):
    param = param.lower()
    status = None
    if param == "healthcare":
        status = healthcare()
    if param == "education":
        status = education()
    if param == "housing":
        status = housing()
    return Response(status, status=200)
    

@app.route("/<model>/<instance>")
def model_instances(model, instance):
    session = DBsession()
    # instances = session.query(Health).filter_by(instance = instance, city_id = city_id).first()
    
    model_class = check_model(model)
    if model_class == None:
        status = ("Model Class Not Found")
        session.close()
        return Response(status, status=404)
    
    model_instance = session.get(model_class, instance)

    if model_instance == None:
        status = ("Instance Not Found")
        session.close()
        return Response(status, status=404)
    else:
        session.close()
        return model_instance.as_dict()

@app.route("/<model>")
def model_page(model):
    ret_obj = {}
    session = DBsession()
    
    model_class = check_model(model)
    if model_class == None:
        status = ("Model Class Not Found")
        session.close()
        return Response(status, status=404)
    
    instances = session.query(model_class).all()
    model_list = []
    for instance in instances:
        model_list.append(instance.as_dict())

    ret_obj[model] = model_list
    session.close()
    return ret_obj

@app.route("/cities")
def get_cities():
    ret_obj = {}
    session = DBsession()
    cities = session.query(City).all()
    city_list = []
    for city in cities:
        city_list.append(city.city_name)

    ret_obj['city'] = city_list
    session.close()
    return ret_obj
    
@app.route("/cities/<id>")
def get_city_name(id):
    session = DBsession()
    city = session.get(City, id)

    if city == None:
        status = ("City Not Found")
        session.close()
        return Response(status, status=404)
    else:
        session.close()
        return city.as_dict()

#testing push
@app.route("/search/<model>/<search>")
def search(model, search):
    session = DBsession()
    
    ret_list = []
    search_list = []
    if model == "all":
        search_list.append(session.query(Health).all())
        search_list.append(session.query(House).all())
        search_list.append(session.query(Education).all())
    elif model == "housing":
        search_list.append(session.query(House).all())
    elif model == "healthcare":
        search_list.append(session.query(Health).all())
    elif model == "education":
        search_list.append(session.query(Education).all())
    
    for values in search_list:
        for obj in values:
            instance = obj.as_dict()
            match_ratio = max(fuzz.partial_ratio(search.lower(), str(val).lower()) for val in instance.values())
            if match_ratio > 80: 
                ret_list.append((instance, match_ratio))
    
    # Sort results by relevance score (match ratio)
    ret_list.sort(key=lambda x: x[1], reverse=True)
    ret_list = [item[0] for item in ret_list] 
    
    ret_obj = {}    
    ret_obj['search'] = ret_list
    session.close()
    return ret_obj

#const response = await axios.post(baseURL + '/filter/', { label, selectedFilters });

#   const filterOptions: { label: string; items: string[] }[] = [
#     { label: 'Cities', items: ['Austin', 'Dallas', 'Houston'] },
#     { label: 'Degree Type', items: ['4 year program'] },
#     { label: 'Resource Type', items: ['Education', 'Employment'] }
#   ];
@app.route("/<model>/filter/sort/", methods=['POST'])
def filter_sort(model):
    model_class = check_model(model)
    if model_class == None:
        status = ("Model Class Not Found")
        session.close()
        return Response(status, status=404)

    master_list = request.json.get('masterFilterList')
    ascending = request.json.get('ascendingOrder')
    for key in master_list.keys():
        print(key, master_list[key])
    session = DBsession()
    query = session.query(model_class)

    if model_class is Education:
        print("Querying education")
        query = filter_education(query, master_list)
    elif model_class is House:
        print("Querying Housing")
        query = filter_housing(query, master_list)
    elif model_class is Health:
        print("Querying Health")
        query = filter_health(query, master_list)

    response = query.all()

    filter_list = []
    
    for obj in response:
        filter_list.append(obj.as_dict())

    ret_list = {}
    
    filter_list = sorted(filter_list, key=lambda x: x.get('name', '').lower())
    if(not ascending):
        filter_list = list(reversed(filter_list))
    ret_list["filter"] = filter_list
    print(ret_list)
    return ret_list

def filter_education(query, master_list):
    for key in master_list.keys():
        if len(master_list[key]) == 0:
            continue
        if key == 'Cities':
            cur_cities = []
            for id, value in city_list.items():
                # print(master_list[key])
                for city in master_list[key]:
                    if city == value:
                        cur_cities.append(id)
            query = query.filter(Education.city_id.in_(cur_cities))
        if key == 'Degree Type':
            query = query.filter(Education.degree.in_(master_list[key]))
        if key == 'Has website':
            if len(master_list[key]) != 2:
                if master_list[key][0] == 'No':
                    query = query.filter(House.link == "")
                else:
                    query = query.filter(not_(House.link == ""))
        if key == 'Funding for':
            string_options = []
            master_query = None
            numberInstances = query.filter(cast(Education.funding, Integer) is not None, Education.funding != "All eligible students", Education.funding != "GI Bill (888-442-4551)")
            for option in master_list[key]:
                # print("this is option ", option)
                if option == ">1000": # 1000
                    numberInstancesAbove1000 = numberInstances.filter(cast(Education.funding, Integer) >= 1000)
                    master_query = numberInstancesAbove1000 if master_query is None else master_query.union(numberInstancesAbove1000)
                elif option == "<1000": #less than 1000
                    numberInstancesBelow1000 = numberInstances.filter(cast(Education.funding, Integer) < 1000)
                    master_query = numberInstancesBelow1000 if master_query is None else master_query.union(numberInstancesBelow1000)
                else: # gathering all non numbers
                    string_options.append(option)
            if len(string_options) != 0: 
                print("string options: ", string_options)
                stringInstances = query.filter(Education.funding.in_(string_options))
                master_query = stringInstances if master_query is None else master_query.union(stringInstances)
            if master_query is not None:
                query = master_query
    return query

def filter_health(query, master_list):
    for key in master_list.keys():
        if len(master_list[key]) == 0:
            continue
        if key == 'Cities':
            cur_cities = []
            for id, value in city_list.items():
                # print(master_list[key])
                for city in master_list[key]:
                    if city == value:
                        cur_cities.append(id)
            query = query.filter(Health.city_id.in_(cur_cities))
        if key == 'Facility Type':
            query = query.filter(Health.facility_type.in_(master_list[key]))
        if key == 'Type of support':
            query = query.filter(Health.assistance_type.in_(master_list[key]))
        if key == 'Has contact':
            if len(master_list[key]) != 2:
                if master_list[key][0] == 'No':
                    query = query.filter(Health.phone == "N/A")
                else:
                    query = query.filter(not_(Health.phone == "N/A"))
    return query

def filter_housing(query, master_list):
    for key in master_list.keys():
        if len(master_list[key]) == 0:
            continue
        if key == 'Cities':
            cur_cities = []
            for id, value in city_list.items():
                for city in master_list[key]:
                    if city == value:
                        cur_cities.append(id)
            query = query.filter(House.city_id.in_(cur_cities))
        if key == 'Eligibility':
            # query = query.filter(House.eligibility_requirements.in_(master_list[key]))
            master_query = None  
            for option in master_list[key]:
                if option == "VETERAN":
                    veteranInstances = query.filter(func.lower(House.eligibility_requirements).contains(func.lower("veteran")))
                    master_query = veteranInstances if master_query is None else master_query.union(veteranInstances)
                elif option == "ANYONE":
                    veteranInstances = query.filter(func.lower(House.eligibility_requirements).contains(func.lower("anyone")))
                    master_query = veteranInstances if master_query is None else master_query.union(veteranInstances)
                elif option == "ADULTS":
                    veteranInstances = query.filter(func.lower(House.eligibility_requirements).contains(func.lower("adult")))
                    master_query = veteranInstances if master_query is None else master_query.union(veteranInstances)
                elif option == "LOW INCOME":
                    veteranInstances = query.filter(func.lower(House.eligibility_requirements).contains(func.lower("income")))
                    master_query = veteranInstances if master_query is None else master_query.union(veteranInstances)
                elif option == "RESIDENT":
                    veteranInstances = query.filter(func.lower(House.eligibility_requirements).contains(func.lower("RESIDENT")))
                    master_query = veteranInstances if master_query is None else master_query.union(veteranInstances)
                elif option == "SENIORS":
                    veteranInstances = query.filter(func.lower(House.eligibility_requirements).contains(func.lower("senior")))
                    master_query = veteranInstances if master_query is None else master_query.union(veteranInstances)
                elif option == "DISABLED":
                    veteranInstances = query.filter(func.lower(House.eligibility_requirements).contains(func.lower("DISABLED")))
                    master_query = veteranInstances if master_query is None else master_query.union(veteranInstances)
                else: # option == "OTHER"
                    # Create a filter that excludes entries already matched by previous conditions
                    otherInstances = query.filter(~(
                        func.lower(House.eligibility_requirements).contains(func.lower("veteran")) |
                        func.lower(House.eligibility_requirements).contains(func.lower("anyone")) |
                        func.lower(House.eligibility_requirements).contains(func.lower("adult")) |
                        func.lower(House.eligibility_requirements).contains(func.lower("income")) |
                        func.lower(House.eligibility_requirements).contains(func.lower("resident")) |
                        func.lower(House.eligibility_requirements).contains(func.lower("senior")) |
                        func.lower(House.eligibility_requirements).contains(func.lower("disabled"))
                    ))
                    
                    # Union with master_query if it's not None
                    master_query = otherInstances if master_query is None else master_query.union(otherInstances)
            if master_query is not None:
                query = master_query
        if key == 'Has website':
            if len(master_list[key]) != 2:
                if master_list[key][0] == 'No':
                    query = query.filter(House.link == "")
                else:
                    query = query.filter(not_(House.link == ""))
        if key == 'Has contact':
            if len(master_list[key]) != 2:
                if master_list[key][0] == 'No':
                    query = query.filter(House.phone == "N/A")
                else:
                    query = query.filter(not_(House.phone == "N/A"))
    return query


@app.route("/housing/visualize/")
def visualize_housing():
    session = DBsession()
    query = session.query(House)
    ret_list = {}
    veteranInstances = query.filter(func.lower(House.eligibility_requirements).contains(func.lower("veteran")))
    ret_list['Veterans'] = veteranInstances.count()
    veteranInstances = query.filter(func.lower(House.eligibility_requirements).contains(func.lower("anyone")))
    ret_list['All'] = veteranInstances.count()
    veteranInstances = query.filter(func.lower(House.eligibility_requirements).contains(func.lower("adult")))
    ret_list['Adults'] = veteranInstances.count()
    veteranInstances = query.filter(func.lower(House.eligibility_requirements).contains(func.lower("income")))
    ret_list['Low-income'] = veteranInstances.count()
    veteranInstances = query.filter(func.lower(House.eligibility_requirements).contains(func.lower("RESIDENT")))
    ret_list['Residents'] = veteranInstances.count()
    veteranInstances = query.filter(func.lower(House.eligibility_requirements).contains(func.lower("senior")))
    ret_list['Seniors'] = veteranInstances.count()
    veteranInstances = query.filter(func.lower(House.eligibility_requirements).contains(func.lower("DISABLED")))
    ret_list['Disabled'] = veteranInstances.count()
    otherInstances = query.filter(~(
        func.lower(House.eligibility_requirements).contains(func.lower("veteran")) |
        func.lower(House.eligibility_requirements).contains(func.lower("anyone")) |
        func.lower(House.eligibility_requirements).contains(func.lower("adult")) |
        func.lower(House.eligibility_requirements).contains(func.lower("income")) |
        func.lower(House.eligibility_requirements).contains(func.lower("resident")) |
        func.lower(House.eligibility_requirements).contains(func.lower("senior")) |
        func.lower(House.eligibility_requirements).contains(func.lower("disabled"))
    ))
    ret_list['Other'] = otherInstances.count()
    print(ret_list)
    return ret_list



@app.route("/healthcare/visualize/")
def visualize_healthcare():
    session = DBsession()
    query = session.query(Health)
    ret_list = {}
    options = session.query(Health).all()
    for instance in options:
        instanceType = instance.facility_type
        if instanceType in ret_list:
            val = ret_list[instanceType]
            ret_list[instanceType] = val + 1
        else:
            ret_list[instanceType] = 1
    print(ret_list)
    ret_list.pop("General Health Care")
    return ret_list


@app.route("/education/visualize/")
def visualize_education():
    session = DBsession()
    query = session.query(Education)
    ret_list = {}
    all_instances = session.query(Education).filter(Education.city_id != 0).all()
    coordinates_list = [{'latitude': instance.latitude, 'longitude': instance.longitude} for instance in all_instances]
    ret_list['coordinates'] = coordinates_list
    print("education visualize")
    print(ret_list)
    return ret_list



##########################################################################################################################################
##########################################################################################################################################
if __name__ == "__main__":
    app.run(port=5009, debug=True)